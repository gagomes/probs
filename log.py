#!/usr/bin/python

import sys
import os
from ipdb import set_trace as dbg
from collections import defaultdict

def main():
    store = defaultdict(list)

    with open('loadavg2uri.log') as f:
        for s in f:
            s = s.strip()
            if s == '':
                continue

            time, uri = s.strip().split()
            store[uri].append(float(time))

        store = { k: sum(store[k]) / len(store[k]) for k in store }

        print  store
        top = sorted(store)[0]
        print top, store[top]

if __name__ == '__main__':
    main()

